import {useState} from "react";
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import {
  MainContainer,
  ChatContainer,
  MessageList,
  Message,
  MessageInput,
  TypingIndicator,
} from "@chatscope/chat-ui-kit-react";

const API_KEY = "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";

const ChatBot = () => {
  const [typing, setTyping] = useState(false);
  const [messages, setMessages] = useState([
    {
      message: "Hello! I am Alexa. How can I assist you today?",
      sender: "chatGPT",
    },
  ]);

  async function processMessageToChatGpt(chatMessages) {
    let apiMessages = chatMessages.map((messageObject) => {
      let role = "";
      if (messageObject.sender === "chatGPT") {
        role = "assistant";
      } else {
        role = "user";
      }
      return {role, content: messageObject.message};
    });

    const systemMessage = {
      role: "system",
      content:
        "Explain products of the groups headphones, keyboards, mouses and smart watches.",
    };

    const apiRequestBody = {
      model: "gpt-3.5-turbo",
      messages: [systemMessage, ...apiMessages],
    };

    await fetch("https://api.openai.com/v1/chat/completions", {
      method: "POST",
      headers: {
        Authorization: Bearer$
    {
      API_KEY
    }
  ,
    "Content-Type"
  :
    "application/json",
  },
    body: JSON.stringify(apiRequestBody),
  })
  .
    then((data) => {
      return data.json();
    })
      .then((data) => {
        if (
          data.choices && data.choices.length > 0 && data.choices[0].message
        ) {
          setMessages([
            ...chatMessages,
            {
              message: data.choices[0].message.content,
              sender: "chatGPT",
            },
          ]);
        }
        setTyping(false);
      });
  }

  const handleSend = async (message) => {
    const newMessage = {
      message,
      sender: "user",
      direction: "outgoing",
    };

    const newMessages = [...messages, newMessage]; // all the old messages + the new messages
    // update our messages state
    setMessages(newMessages);
    // set a typing indicator
    setTyping(true);
    // process message to chatGPT (send it over and see the response)
    await processMessageToChatGpt(newMessages);
  };

  return (
    <div>
      <h1>Alexa</h1>
      <MainContainer>
        <ChatContainer>
          <MessageList
            typingIndicator={
              typing ? <TypingIndicator content="Alexa is typing"/> : null
            }
          >
            {messages.map((message, i) => {
              return <Message key={i} model={message}/>;
            })}
          </MessageList>
          <MessageInput placeholder="Type message here" onSend={handleSend}/>
        </ChatContainer>
      </MainContainer>
    </div>
  );
};

export default ChatBot;